package ffufm.juveil.api.handlerimpl.post

import ffufm.juveil.api.external.PostService
import ffufm.juveil.api.spec.dbo.post.PostPost
import ffufm.juveil.api.spec.handler.post.PostPostDatabaseHandler
import org.springframework.stereotype.Component


@Component("post.PostPostHandler")
class PostPostHandlerImpl(val postApi : PostService): PostPostDatabaseHandler {
    override suspend fun create(body: PostPost): PostPost {
        return postApi.postPosts(body)
    }
}