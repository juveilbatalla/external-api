package ffufm.juveil.api.handlerimpl.url

import ffufm.juveil.api.external.PostService
import ffufm.juveil.api.spec.dbo.url.UrlUrl
import ffufm.juveil.api.spec.handler.url.UrlUrlDatabaseHandler

import org.springframework.stereotype.Component


@Component("url.UrlUrlHandler")
class UrlUrlHandlerImpl(val postApi : PostService): UrlUrlDatabaseHandler {
    override suspend fun create(body: UrlUrl): String{
        return postApi.postUrl(body)
    }
}