package ffufm.juveil.api.handlerimpl.report

import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.juveil.api.external.PostService
import ffufm.juveil.api.spec.dbo.report.ReportReport
import ffufm.juveil.api.spec.handler.report.ReportReportDatabaseHandler
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("report.ReportReportHandler")
class ReportReportHandlerImpl(val postApi : PostService) : ReportReportDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Return total number of posts from the API
     */
    override suspend fun totalPosts(userId: Long?): ReportReport? {
        val posts = postApi.getPosts(userId)
        val totalPosts = posts.size
        return ReportReport(totalPosts = totalPosts)
    }


}
