package ffufm.juveil.api.external



import ffufm.juveil.api.spec.dbo.post.PostPost
import ffufm.juveil.api.spec.dbo.url.UrlUrl
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody


@Service
class PostService {
    //Establish connection
//    fun postApiClient(): WebClient {
//        return WebClient
//            .builder()
//            .baseUrl("https://jsonplaceholder.typicode.com")
//            .filter { request, next -> next.exchange(request) }
//            .build()
//    }
    fun postApiClient(): WebClient {
        return WebClient
            .builder()
            .baseUrl("https://cleanuri.com")
            .filter { request, next -> next.exchange(request) }
            .build()
    }
    //Get Posts from external API
    suspend fun getPosts(userId: Long?): List<Any> {
        val client = postApiClient()
        return client.get().uri { builder ->
            builder.apply {
                path("/posts")
            }
                .build()
        }
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .retrieve()
            .awaitBody()
    }


    suspend fun  postPosts(post: PostPost): PostPost{
        val client = postApiClient()
        return client.post().uri { builder ->
            builder.apply {
                path("/posts")
            }
                .build()
        }
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .bodyValue(post) //body ng request na ipapasok
            .retrieve()
            .awaitBody()
    }

    suspend fun  postUrl(post: UrlUrl): String{
        val client = postApiClient()
        return client.post().uri { builder ->
            builder.apply {
                path("/api/v1/shorten")
            }
                .build()
        }
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .bodyValue(post)
            .retrieve()
            .awaitBody()
    }



}