package ffufm.juveil.api.external

import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody

//class PostServiceWeather {
//    //Establish connection
//    fun postApiClientWeather(): WebClient {
//        return WebClient
//            .builder()
//            .baseUrl("https://goweather.herokuapp.com")
//            .filter { request, next -> next.exchange(request) }
//            .build()
//    }
//
//    //Get Posts from external API
//    suspend fun getWeatherToday(): String {
//        val client = postApiClientWeather()
//        return client.get().uri { builder ->
//            builder.apply {
//                path("/weather/Manila")
//            }
//                .build()
//        }
//            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
//            .retrieve()
//            .awaitBody()
//    }
//}