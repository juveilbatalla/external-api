package ffufm.juveil.api.spec.dbo.post

import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassModel
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import java.util.TreeSet
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.SequenceGenerator
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType

/**
 * This model will reflect the structure of the External API
 */
data class PostPost(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * This attribute holds the ID of the creator of the post
     */
    val userId: Long? = null,
    /**
     * This attribute will hold the title of the post
     */
    val title: String? = null,
    /**
     * This attribute will hold the body of the post
     */
    val body: String? = null
) : PassModel<PostPost, Long>() {
    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}
