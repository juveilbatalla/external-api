package ffufm.juveil.api.spec.dbo.url

import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassModel
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import java.util.TreeSet
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.SequenceGenerator
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType

/**
 * This represent the url that will request
 */
data class UrlUrl(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Url that will use
     */
    val url: String? = null
) : PassModel<UrlUrl, Long>() {
    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}
