package ffufm.juveil.api.spec.handler.report

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.juveil.api.spec.dbo.report.ReportReport
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface ReportReportDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Return total number of posts from the API
     */
    suspend fun totalPosts(userId: Long? = null): ReportReport?
}

@Controller("report.Report")
class ReportReportHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: ReportReportDatabaseHandler

    /**
     * : 
     * HTTP Code 200: Return total number of posts from the API
     */
    @RequestMapping(value = ["/reports/"], method = [RequestMethod.GET])
    suspend fun totalPosts(@RequestParam("userId") userId: Long?): ResponseEntity<*> {

        return success { databaseHandler.totalPosts(userId) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }
}
