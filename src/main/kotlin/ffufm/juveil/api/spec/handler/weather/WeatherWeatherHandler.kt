package ffufm.juveil.api.spec.handler.weather

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.juveil.api.spec.dbo.weather.WeatherWeather
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface WeatherWeatherDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: It returns todays weather
     */
    suspend fun getWeatherToday(): WeatherWeather?
}

@Controller("weather.Weather")
class WeatherWeatherHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: WeatherWeatherDatabaseHandler

    /**
     * : 
     * HTTP Code 200: It returns todays weather
     */
    @RequestMapping(value = ["/weather-today/"], method = [RequestMethod.GET])
    suspend fun getWeatherToday(): ResponseEntity<*> {

        return success { databaseHandler.getWeatherToday() ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }
}
