package ffufm.juveil.api.spec.handler.url

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.juveil.api.spec.dbo.url.UrlUrl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UrlUrlDatabaseHandler {
    /**
     * Create Url: Creates a new Url object
     * HTTP Code 201: The created Url
     */
    suspend fun create(body: UrlUrl): String
}

@Controller("url.Url")
class UrlUrlHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UrlUrlDatabaseHandler

    /**
     * Create Url: Creates a new Url object
     * HTTP Code 201: The created Url
     */
    @RequestMapping(value = ["/urls/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: UrlUrl): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }
}
