package ffufm.juveil.api.spec.handler.post

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.juveil.api.spec.dbo.post.PostPost
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface PostPostDatabaseHandler {
    /**
     * Create Post: Creates a new Post object
     * HTTP Code 201: The created Post
     */
    suspend fun create(body: PostPost): PostPost
}

@Controller("post.Post")
class PostPostHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: PostPostDatabaseHandler

    /**
     * Create Post: Creates a new Post object
     * HTTP Code 201: The created Post
     */
    @RequestMapping(value = ["/posts/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: PostPost): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }
}
