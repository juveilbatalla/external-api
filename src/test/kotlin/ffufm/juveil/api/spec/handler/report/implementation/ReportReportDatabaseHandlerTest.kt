package ffufm.juveil.api.spec.handler.report.implementation

import ffufm.juveil.api.PassTestBase
import ffufm.juveil.api.spec.dbo.report.ReportReport
import ffufm.juveil.api.spec.handler.report.ReportReportDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class ReportReportDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var reportReportDatabaseHandler: ReportReportDatabaseHandler

    @Test
    fun `test totalPosts`() = runBlocking {
        reportReportDatabaseHandler.totalPosts()
        Unit
    }
}
